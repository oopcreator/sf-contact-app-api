<?php

namespace App\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use App\Event\ContactCreatedEvent;
use App\Service\ContactManager;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Mime\Address;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use App\Exception\MailerException;

class ContactSubscriber implements EventSubscriberInterface
{
    /**
     * @var MailerInterface $mailer
     */
    private $mailer;

    /**
     * @var ContactManager
     */
    private $contactManager;

    /**
     *
     * @param ContactManager  $contactManager
     * @param MailerInterface $mailer
     */
    public function __construct(ContactManager $contactManager, MailerInterface $mailer)
    {
        $this->contactManager = $contactManager;
        $this->mailer = $mailer;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            ContactCreatedEvent::NAME => 'onContactCreated',
        ];
    }

    /**
     * @param ContactCreatedEvent $event
     */
    public function onContactCreated(ContactCreatedEvent $event)
    {
        if (empty($event->getContact()) || $event->getContact()->getSent()) {
            return;
        }

        $contact = $event->getContact();

        $email = (new TemplatedEmail())
            ->from($contact->getEmail())
            ->to(new Address($_ENV['EMAIL_CONTACT_TO']))
            ->subject($_ENV['EMAIL_CONTACT_SUBJECT'])
            ->htmlTemplate($_ENV['EMAIL_CONTACT_TEMPLATE'])
            ->context([
                'contact' => $contact,
            ])
        ;

        // TODO:
        //  1. Make this ASYNCHRONOUS or run via BACKGROUND PROCESS OR thru MESSAGING e.g Queued (RabbitMq)
        //  2. LOG any possible failure and successful return to DB
        try {
            $this->mailer->send($email);
            $contact->setSent(true);
            $this->contactManager->update($contact);
        } catch (TransportExceptionInterface $e) {
            throw MailerException::transportFails($e->getCode());
        }

    }
}