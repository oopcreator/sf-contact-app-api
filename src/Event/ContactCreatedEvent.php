<?php

namespace App\Event;

use Symfony\Component\EventDispatcher\GenericEvent;
use App\Entity\Contact;

/**
 * Class ContactCreatedEvent
 * @package App\Event
 */
class ContactCreatedEvent extends GenericEvent
{
    const NAME = 'contact.created';

    /**
     * @var Contact
     */
    protected $contact;

    /**
     * @param Contact $contact
     */
    public function __construct(Contact $contact)
    {
        $this->contact = $contact;
    }

    /**
     * @return Contact
     */
    public function getContact()
    {
        return $this->contact;
    }
}