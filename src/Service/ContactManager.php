<?php

namespace App\Service;

use App\Entity\Contact;
use App\Repository\ContactRepository;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use App\Event\ContactCreatedEvent;

class ContactManager extends BaseManager
{

    /**
     * @var EventDispatcher
     */
    private $dispatcher;

    /**
     * @var ContactRepository
     */
    public $repository;

    /**
     * ContactManager constructor.
     *
     * @param ContactRepository $repository
     */
    public function __construct(ContactRepository $repository, EventDispatcherInterface $dispatcher)
    {
        $this->repository = $repository;
        $this->dispatcher = $dispatcher;
    }

    /**
     * Create Contact
     * @param array $params
     * @return Contact
     * @throws \Exception
     */
    public function create(array $params)
    {
        $contact = $this->parse(new Contact(), $params);
        $contact->setCreatedAt(new \DateTime());
        $this->save($contact);

        $this->dispatcher->dispatch(new ContactCreatedEvent($contact), ContactCreatedEvent::NAME);

        return $contact;
    }

    /**
     * Update Contact
     * @param Contact $contact
     * @return Contact
     * @throws \Exception
     */
    public function update(Contact $contact)
    {
        $this->save($contact);
        return $contact;
    }

    /**
     * @param Contact  $client
     * @param array $params
     * @return Contact
     */
    private function parse(Contact $contact, array $params = null)
    {
        if (isset($params["lname"])) {
            $contact->setLname($params["lname"]);
        }

        if (isset($params["fname"])) {
            $contact->setFname($params["fname"]);
        }

        if (isset($params["email"])) {
            $contact->setEmail($params["email"]);
        }

        if (isset($params["message"])) {
            $contact->setMessage($params["message"]);
        }

        return $contact;
    }

    /**
     * Get All Contacts
     *
     * @return Contact[]
     */
    public function getAll()
    {
        return $this->repository->findAll();
    }

}
