<?php

/**
 * This Class Serves as Service Handler for Salesforce API call
 */
namespace App\Service;

use App\Entity\Contact;
use http\Env\Response;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;

class SalesforceService
{
    /**
     * @var HttpClientInterface
     */
    private $client;

    /**
     * @var string
     */
    private $token;

    /**
     * @var string
     */
    private $refreshToken;

    /**
     * SalesforceService constructor.
     * @param HttpClientInterface $client
     */
    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * Authenticate to Salesforce
     * This send login credentials
     */
    public function authenticate() : TOKEN
    {
        $metaData = [
            'clientID' => $_ENV['clientID'],
            'clientSecret' => $_ENV['clientSecret'],
            'username' => $_ENV['SfUsername'],
            'password' => $_ENV['SfPassword']
        ];

        try {
            $response = $this->client->request('POST', 'URL', $metaData);
        } catch (TransportExceptionInterface $e) {
            // TODO:
            //  1. LOG the ERROR to DB
            //  2. THROW ERROR
            throw APICallException::transportFails($e->getCode());
        }

        $this->token = $response->token;
        $this->refreshToken = $response->refreshToken;
    }

    public function getToken()
    {
        return $this->token;
    }

    public function getRefreshToken()
    {
        $this->refreshToken;
    }

    /**
     * Request Token by Refresh Token
     */
    public function RequestTokenByRefreshToken($token)
    {
        $metaData = [
            'token' => $token,
        ];

        try {
            $response = $this->client->request('POST', 'URL', $metaData);
        } catch (TransportExceptionInterface $e) {
            // TODO:
            //  1. LOG the ERROR to DB
            //  2. THROW ERROR
            throw APICallException::transportFails($e->getCode());
        }


        $this->token = $response->token;
        $this->refreshToken = $response->refreshToken;
    }

    /**
     * Post Contact
     *
     * @param Contact $contact
     * @throws @APICallException
     * @return Response
     */
    public function postContact(Contact $contact)
    {
        try {
            $response = $this->client->request('POST', 'URL', $contact);
        } catch (TransportExceptionInterface $e) {
            // TODO:
            //  1. LOG the ERROR to DB
            //  2. THROW ERROR
            throw APICallException::transportFails($e->getCode());
        }

        return $response;
    }

    /**
     * GET Contact
     *
     * @param Contact $contact
     * @throws @APICallException
     * @return Response
     */
    public function getContact($limit = 10, $offset= 0)
    {
        $params = [
            'limit' => $limit,
            'offet' => $offset
        ];

        try {
            $response = $this->client->request('GET', 'URL', $params);
        } catch (TransportExceptionInterface $e) {
            // TODO:
            //  1. LOG the ERROR to DB
            //  2. THROW ERROR
            throw APICallException::transportFails($e->getCode());
        }

        return $response;
    }
}